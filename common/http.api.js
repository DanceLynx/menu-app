import httpPath from './http.path'

const install = (Vue,vm) => {
	
	let api = {}
	api.tab = {
		index: (options) => vm.$u.get(httpPath.tab.index.url,options),// 获取tab标签
	}
	api.carousel = {
		index: (options) => vm.$u.get(httpPath.carousel.index.url,options), // 获取轮播图
	}
	api.menu = {
		index: (options) => vm.$u.get(httpPath.menu.index.url,options), // 获取菜单列表
		show: (options) => vm.$u.get(httpPath.menu.show.url,options), // 获取菜单内容
	}
	api.product = {
		index: (options) => vm.$u.get(httpPath.product.index.url,options), // 获取产品列表
		show: (options) => vm.$u.get(httpPath.product.show.url,options), // 获取产品内容
	}
	api.category = {
		index: (options={}) => vm.$u.get(httpPath.category.index.url,options),// 获取所有分类
	}
	api.user = {
		register: (options={}) => vm.$u.post(httpPath.user.register.url,options),// 用户注册
		login: (options={}) => vm.$u.post(httpPath.user.login.url,options),//用户登录
		checkMobile: (options={}) => vm.$u.get(httpPath.user.checkMobile.url,options),// 检测手机号是否存在
		checkUsername: (options={}) => vm.$u.get(httpPath.user.checkUsername.url,options),// 检测用户名是否存在
		addAddress: (options={}) => vm.$u.post(httpPath.user.addAddress.url,options), // 添加收货地址
		indexAddress: (options={}) => vm.$u.post(httpPath.user.indexAddress.url,options), // 收货地址列表
		delAddress: (options={}) => vm.$u.get(httpPath.user.delAddress.url,options),//删除收货地址
		editAddress: (options={}) => vm.$u.post(httpPath.user.editAddress.url,options),//修改收货地址
	}
	api.other = {
		captcha: () => vm.$u.get(httpPath.other.captcha.url),// 获取图片验证码
	}
	Vue.prototype.$api = api
}
export default {install}