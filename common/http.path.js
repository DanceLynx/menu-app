module.exports = {
	tab: {
		index: {
			url: 'api.tab/index',
			auth: false
		}
	},
	carousel: {
		index: {
			url: 'api.carousel/index',
			auth: false
		}
	},
	menu: {
		index: {
			url: 'api.menu/index',
			auth: false
		},
		show: {
			url: 'api.menu/show',
			auth: false
		}
	},
	product: {
		index: {
			url: 'api.product/index',
			auth: false
		},
		show: {
			url: 'api.product/show',
			auth: false
		}
	},
	category: {
		index: {
			url: 'api.category/index',
			auth: false
		}
	},
	user: {
		register: {
			url: 'api.user/register',
			auth: false
		},
		login: {
			url: 'api.user/login',
			auth: false
		},
		checkUsername: {
			url: 'api.validate/check_username_available',
			auth: false
		},
		checkMobile: {
			url: 'api.validate/check_mobile_available',
			auth: false
		},
		addAddress: {
			url: 'api.address/add',
			auth: true
		},
		editAddress: {
			url: 'api.address/edit',
			auth: true
		},
		indexAddress: {
			url: 'api.address/index',
			auth: true
		},
		delAddress: {
			url: 'api.address/del',
			auth: true
		}
	},
	other: {
		captcha: {
			url: 'api.index/captcha',
			auth: false
		}
	}
}