import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import HttpCache from '@/common/cache.js'
let lifeData = {};

try{
	// 尝试获取本地是否存在lifeData变量，第一次启动APP时是不存在的
	lifeData = uni.getStorageSync('lifeData');
}catch(e){
	
}

// 需要永久存储，且下次APP启动需要取出的，在state中的变量名

// 保存变量到本地存储中
const saveLifeData = function(key, value){
	// 判断变量名是否在需要存储的数组中
	if(saveStateKeys.indexOf(key) != -1) {
		// 获取本地存储的lifeData对象，将变量添加到对象中
		let tmp = uni.getStorageSync('lifeData');
		// 第一次打开APP，不存在lifeData变量，故放一个{}空对象
		tmp = tmp ? tmp : {};
		tmp[key] = value;
		// 执行这一步后，所有需要存储的变量，都挂载在本地的lifeData对象中
		uni.setStorageSync('lifeData', tmp);
	}
}
let menuCategory = HttpCache.get('menu_category')
let productCategory = HttpCache.get('product_category')
let userinfo = HttpCache.get('userinfo')

const store = new Vuex.Store({
	state: {
		vuex_tabbar: [
			{
				iconPath: "home",
				selectedIconPath: "home-fill",
				text: '首页',
				pagePath: '/pages/index/index'
			},
			{
				iconPath: "shopping-cart",
				selectedIconPath: "shopping-cart-fill",
				text: '食材',
				pagePath: '/pages/product/index'
			},
			{
				iconPath: "account",
				selectedIconPath: "account-fill",
				text: "我的",
				pagePath: '/pages/user/index'
			},
		],
		menuCategory: menuCategory?menuCategory:null,
		productCategory: productCategory?productCategory:null,
		userinfo: userinfo?userinfo:{},
		loginTip: false
	},
	mutations: {
		$uStore(state, payload) {
			// 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
			let nameArr = payload.name.split('.');
			let saveKey = '';
			let len = nameArr.length;
			if(len >= 2) {
				let obj = state[nameArr[0]];
				for(let i = 1; i < len - 1; i ++) {
					obj = obj[nameArr[i]];
				}
				obj[nameArr[len - 1]] = payload.value;
				saveKey = nameArr[0];
			} else {
				// 单层级变量，在state就是一个普通变量的情况
				state[payload.name] = payload.value;
				saveKey = payload.name;
			}
			// 保存变量到本地，见顶部函数定义
			saveLifeData(saveKey, state[saveKey])
		},
		saveMenuCategory(state,data){
			HttpCache.put('menu_category',data)
			state.menuCategory = data
		},
		saveProductCategory(state,data){
			HttpCache.put('product_category',data)
			state.productCategory = data
		},
		saveUserinfo(state,data){
			HttpCache.put('userinfo',data)
			state.userinfo = data
		},
		clearUserinfo(state){
			HttpCache.remove('userinfo')
			state.userinfo = {}
		},
		setLoginTip(state,data){
			state.loginTip = data
		}
	},
	getters:{
		token(state){
			if(state.userinfo.hasOwnProperty('token'))
				return state.userinfo.token
			return ''
		},
		isLogin(state){
			return state.userinfo.hasOwnProperty('token')
		}
	},
	actions: {
		isInPage({
			state
		},path){
			return new Promise((resolve,reject)=>{
				let has = false
				state.vuex_tabbar.forEach((item)=>{
					if(item.pagePath == path.split('?')[0])
						has = true
				})
				has ? resolve() : reject()
			})
		}
	}
})

export default store
