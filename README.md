# 闪电橙菜谱APP

#### 使用说明
直接导入hbuilder X 就可以用了！需要安装scss相关插件。

鸣谢以下项目:

* [uView: uView UI，是uni-app生态最优秀的UI框架，全面的组件和便捷的工具会让您信手拈来，如鱼得水 (gitee.com)](https://gitee.com/xuqu/uView)
* [dcloudio/uni-app: uni-app 是使用 Vue 语法开发小程序、H5、App的统一框架 (github.com)](https://github.com/dcloudio/uni-app)
* [uni-simple-router](https://github.com/SilurianYang/uni-simple-router)

<div style="display:flex;flex-wrap:wrap;">
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125121844.png" alt="首页" style="zoom:50%;" />
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125121929.png" alt="菜谱列表" style="zoom:50%;" />
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125122009.png" alt="菜谱详情" style="zoom:50%;" />
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125122058.png" alt="商品列表" style="zoom:50%;" />
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125122126.png" alt="商品详情" style="zoom:50%;" />
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125122201.png" alt="个人中心" style="zoom:50%;" />
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125122228.png" alt="登录提醒" style="zoom:50%;" />
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125122258.png" alt="用户登录" style="zoom:50%;" />
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125122320.png" alt="用户注册" style="zoom:50%;" />
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125122459.png" alt="收货地址" style="zoom:50%;" />
    <img src="https://gitee.com/DanceLynx/menu-app/raw/dev/example/example_20201125123811.png" alt="添加收货地址" style="zoom:50%;" />
</div>