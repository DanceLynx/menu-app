import modules from './modules'
import Vue from 'vue'
import Router from 'uni-simple-router'
import store from '@/store/index.js'

Vue.use(Router)
//初始化
const router = new Router({
    routes: [...modules]//路由表
});

//全局路由前置守卫
router.beforeEach((to, from, next) => {
	if(to.meta && to.meta.auth && !store.getters.isLogin){
		store.commit('setLoginTip',true)
		next(false)
	}else{
		next()
	}
})
// 全局路由后置守卫
router.afterEach((to, from) => {
})
export default router;