const user = [
	{
		path: '/pages/user/index',
		name: 'user.index',
		meta: {
			title: '用户中心',
		},
	},
	{
		path: '/pages/user/login',
		name: 'user.login',
		meta: {
			title: '用户登录',
		},
	},
	{
		path: '/pages/user/register',
		name: 'user.register',
		meta: {
			title: '用户注册',
		},
	},
	{
		path: '/pages/user/setting',
		name: 'user.setting',
		meta: {
			title: '设置中心',
			auth: true
		},
	},
	{
		path: '/pages/user/address-list',
		name: 'user.address-list',
		meta: {
			title: '收货地址',
			auth: true
		},
	},
	{
		path: '/pages/user/address-edit',
		name: 'user.address-edit',
		meta: {
			title: '收货地址编辑',
			auth: true
		},
	},
]
export default user