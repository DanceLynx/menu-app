export default [
	{
		path: '/pages/product/detail',
		name: 'product.detail',
		meta: {
			title: '产品详情'
		}
	},
	{
		path: '/pages/product/index',
		name: 'product.index',
		meta: {
			title: '产品列表'
		}
	}
]