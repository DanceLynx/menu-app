export default [
	{
		path: '/pages/menu/detail',
		name: 'menu.detail',
		meta: {
			title: '菜单详情'
		}
	},
	{
		path: '/pages/menu/index',
		name: 'menu.index',
		meta: {
			title: '菜单列表'
		}
	}
]