export default {
	methods: {
		loadmenuCategory(){
			return this.$api.category.index()
			.then(res=>{
				if(res.code == 1)
					this.$store.commit('saveMenuCategory',res.data)
			})
		},
		loadProductCategory(){
			return this.$api.category.index({
				type: 'product'
			})
			.then(res=>{
				if(res.code == 1)
					this.$store.commit('saveProductCategory',res.data)
			})
		}
	}
}